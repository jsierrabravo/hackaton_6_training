import 'package:frontend/service/product.dart';

class ResponseDRF {

  List<Product>? response = [];

  ResponseDRF({ this.response });

  ResponseDRF.fromJson(List<dynamic> products) {
    
    products.map((product) {
      
      response!.add(Product.fromJson(product));
    });
    

  }

}

// Product.fromJson(jsonDecode(response.body)["results"])