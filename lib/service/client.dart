import 'dart:convert';
import 'package:http/http.dart' as http;

class ApiClient {
  static const baseUrl = 'http://192.168.1.61:8000/api/products';
  static const headers = {"content-type": "application/json"};

  void deleteProduct(int id) async {
    await http.delete(Uri.parse('$baseUrl/$id/'));
    // updateState();
  }

  void createProduct(String name, String price) async {
    await http.post(Uri.parse(baseUrl),
        body: jsonEncode({
          'name': name,
          'price': int.parse(price),
          'image': 'https://picsum.photos/200/300',
        }),
        headers: headers);
    // updateState();
  }

  void updateProduct(int id, String name, String price) async {
    await http.put(Uri.parse('$baseUrl/$id/'),
        body: jsonEncode({
          'name': name,
          'price': int.parse(price),
          'image': 'https://picsum.photos/200/300',
        }),
        headers: headers);
    // updateState();
  }
}
