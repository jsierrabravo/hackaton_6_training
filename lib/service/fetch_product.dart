import 'dart:convert';

import 'package:frontend/service/product.dart';
import 'package:http/http.dart' as http;

Future<List<Product>> fetchProduct() async {
  final response = await http
      .get(Uri.parse('http://192.168.1.75:8000/api/products/'));

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    List<dynamic> results =
        jsonDecode(utf8.decode(response.bodyBytes))["results"];
    return results.map((result) => Product.fromJson(result)).toList();
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load products');
  }
}
