class Product {
  final int id;
  final String name;
  final String price;
  final String image;
  final String url;

  const Product({
    required this.id,
    required this.name,
    required this.price,
    required this.image,
    required this.url,
  });

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      id: json['id'],
      name: json['name'],
      price: json['price'].toString(),
      image: json['image'],
      url: json['url'],
    );
  }
}