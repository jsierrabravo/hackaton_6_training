import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:frontend/view/add_product_form.dart';
import 'package:frontend/view/update_product_form.dart';

import '../service/product.dart';
import 'package:http/http.dart' as http;

import '../service/client.dart';

class ProductList extends StatefulWidget {
  const ProductList({Key? key}) : super(key: key);

  @override
  State<ProductList> createState() => _ProductListState();
}

class _ProductListState extends State<ProductList> {
  final controller = ScrollController();
  List<Product> futureProduct = [];
  int page = 1;
  bool hasMore = true;
  ApiClient client = ApiClient();

  void updateState() async {
    setState(() {
      page = 1;
      hasMore = true;
      futureProduct = [];
    });
    await fetchPages();
  }

  @override
  void initState() {
    super.initState();
    fetchPages();
    controller.addListener(() {
      if (controller.position.maxScrollExtent == controller.offset) {
        fetchPages();
      }
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Future fetchPages() async {
    if (hasMore){

      final url = Uri.parse('http://192.168.1.75:8000/api/products/?page=$page');
      final response = await http.get(url);
      
      if (response.statusCode == 200) {
        page++;

        // If the server did return a 200 OK response,
        // then parse the JSON.
        final jsonDecoded = jsonDecode(utf8.decode(response.bodyBytes));
        List<dynamic> results = jsonDecoded["results"];
        setState(() {
          if (jsonDecoded["next"] == null) {
            hasMore = false;
          }
          futureProduct.addAll(results.map((item) => Product.fromJson(item)).toList());
        });
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to load products');
      }

    }
    
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(0, 0, 0, 0),
          title: const Text('Home', style: TextStyle(color: Colors.black)),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.only(right: 20.0),
              child: IconButton(
                  icon: const Icon(Icons.add, color: Color(0xFF460505)),
                  tooltip: 'Add product to list',
                  onPressed: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return AlertDialog(
                            content: DialogForm(
                              popDialog: Navigator.of(context).pop,
                              createProduct: client.createProduct,
                            ),
                          );
                        });
                    updateState(); // update the list after adding a new product
                  }),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 20.0),
              child: IconButton(
                  icon: const Icon(Icons.refresh, color: Color(0xFF460505)),
                  tooltip: 'Reload list of products',
                  onPressed: () {
                    updateState();
                  }),
            ),
          ],
        ),
        body: RefreshIndicator(
            onRefresh: () {
              updateState();
              return fetchPages();
            },
            child: ListView.builder(
              controller: controller,
              itemCount: futureProduct.length + 1,
              // physics: const BouncingScrollPhysics(),
              itemBuilder: (context, index) {
                if (index < futureProduct.length) {
                  return Center(
                      child: Card(
                          color: const Color.fromARGB(255, 255, 255, 255),
                          shape: RoundedRectangleBorder(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(12)),
                            side: BorderSide(
                              color: Colors.black.withOpacity(0.2),
                              width: 1,
                            ),
                          ),
                          child: SizedBox(
                              width: 370,
                              height: 120,
                              child: Row(
                                children: <Widget>[
                                  const SizedBox(width: 20),
                                  Expanded(
                                    child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text(futureProduct[index].id.toString() + ". " + futureProduct[index].name,
                                              style: const TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold,
                                              ))
                                        ]),
                                  ),
                                  Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        IconButton(
                                          icon: const Icon(Icons.clear,
                                              color: Color(0xFF460505)),
                                          tooltip: 'Delete item',
                                          onPressed: () {
                                            showDialog(
                                                context: context,
                                                builder:
                                                    (BuildContext context) {
                                                  return AlertDialog(
                                                    title: const Text(
                                                        'Are you sure you want to delete this item?'),
                                                    actions: [
                                                      TextButton(
                                                        onPressed: () {
                                                          Navigator.of(context)
                                                              .pop();
                                                          updateState();
                                                        },
                                                        child: const Text(
                                                            'Cancel'),
                                                      ),
                                                      TextButton(
                                                        onPressed: () {
                                                          client.deleteProduct(
                                                              futureProduct[
                                                                      index]
                                                                  .id);
                                                          Navigator.of(context)
                                                              .pop();
                                                        },
                                                        child: const Text(
                                                            'Confirm'),
                                                      ),
                                                    ],
                                                  );
                                                });
                                          },
                                        ),
                                        IconButton(
                                            icon: const Icon(
                                                Icons.open_in_new_sharp,
                                                color: Color(0xFF460505)),
                                            tooltip: 'Update product',
                                            onPressed: () {
                                              showDialog(
                                                  context: context,
                                                  builder:
                                                      (BuildContext context) {
                                                    return AlertDialog(
                                                      content: DialogUpdateForm(
                                                        popDialog: Navigator.of(
                                                                context)
                                                            .pop,
                                                        product: futureProduct[
                                                            index],
                                                        updateProduct: client
                                                            .updateProduct,
                                                      ),
                                                    );
                                                  });
                                            }),
                                      ]),
                                ],
                              ))));
                } else {
                  return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 32),
                      child: Center(
                        child: hasMore
                            ? const CircularProgressIndicator()
                            : const Text('No more products'),
                      ));
                }
              },
            )));
  }
}
