import 'package:flutter/material.dart';

import '../service/product.dart';

class DialogUpdateForm extends StatefulWidget {

  final Function popDialog;
  final Product product;
  final void Function(int id, String name, String price) updateProduct;

  const DialogUpdateForm({
    super.key, 
    required this.popDialog, 
    required this.product,
    required this.updateProduct,
  });

  @override
  State<DialogUpdateForm> createState() => _DialogUpdateFormState();
}

class _DialogUpdateFormState extends State<DialogUpdateForm> {
  final _priceController = TextEditingController();
  final _nameController = TextEditingController();

  @override
  void dispose() {
    _nameController.dispose();
    _priceController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _nameController.text = widget.product.name;
    _priceController.text = widget.product.price;
  }

  @override
  Widget build(BuildContext context) {

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        const Text(
          'Name',
          textAlign: TextAlign.left,
          style: TextStyle(color: Color.fromARGB(255, 105, 105, 105))
        ),
        SizedBox(
          height: 40,
          child: TextField(
            controller: _nameController,
          ),
        ),
        const SizedBox(
          height: 22,
        ),
        const Text(
          'Price',
          textAlign: TextAlign.left,
          style: TextStyle(color: Color.fromARGB(255, 105, 105, 105))
        ),
        SizedBox(
          height: 40,
          child: TextField(
            controller: _priceController,
            decoration: const InputDecoration(
              label: Text('\$')
            ),
          ),
        ),
        SizedBox(
          height: 40,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextButton(
                onPressed: () {
                  widget.popDialog();
                },
                child: const Text('Cancel'),
              ),
              TextButton(
                onPressed: () {
                  widget.updateProduct(
                    widget.product.id,
                    _nameController.text,
                    _priceController.text,
                  );
                  widget.popDialog();
                },
                child: const Text('Confirm'),
              ),
            ],
          )
        ),
      ]
    );
  }
}